package com.toomuchminecraft.regionaltunes;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.xxmicloxx.NoteBlockAPI.RadioSongPlayer;
import com.xxmicloxx.NoteBlockAPI.Song;
import com.xxmicloxx.NoteBlockAPI.SongPlayer;

public class RegionPlayer {
	private String regionName;
	private boolean random = false;
	private ArrayList<Song> songs = new ArrayList<Song>();

	private int index = 0;
	private SongPlayer songPlayer;

	public RegionPlayer(String regionID) {
		this.regionName = regionID;
		tryStart();
	}

	private boolean tryStart() {
		if (songs.size() > 0 && songPlayer == null) {
			songPlayer = new RadioSongPlayer(getNextSong());
			songPlayer.setAutoDestroy(false);
			songPlayer.setPlaying(true);
			return true;
		} else
			return false;
	}

	public void addSong(Song song) {
		songs.add(song);
		tryStart();
	}

	public void removeSong(Song song) {
		songs.remove(song);
		if (songPlayer.getSong() == song)
			onSongFinished();
	}

	public String getRegion() {
		return regionName;
	}

	public void setShuffle(boolean shuffle) {
		random = shuffle;
	}
	
	public void toggleShuffle() {
		random = !random;
	}

	public boolean getShuffle() {
		return random;
	}

	public ArrayList<Song> getSongList() {
		return songs;
	}

	public SongPlayer getSongPlayer() {
		return songPlayer;
	}

	public boolean contains(Song song) {
		return songs.contains(song);
	}

	public Song getNextSong() {
		if (songs.size() > 0) {
			if (random) {
				Random rand = new Random();
				return songs.get(rand.nextInt(songs.size()));
			} else {
				if (songs.size() > index + 1)
					index++;
				else
					index = 0;
				return songs.get(index);
			}
		} else 
			return null;
	}

	public void addPlayer(Player player) {
		if (player != null) {
			songPlayer.addPlayer(player);

			if (!songPlayer.isPlaying())
				tryStart();
		}
	}

	public void removePlayer(Player player) {
		songPlayer.removePlayer(player);
	}

	public void onSongFinished() {

		SongPlayer sp = new RadioSongPlayer(getNextSong());

		for (String p : songPlayer.getPlayerList()) {
			sp.addPlayer(Bukkit.getPlayerExact(p));
			//songPlayer.removePlayer(Bukkit.getPlayerExact(p));
			
		}
		sp.setAutoDestroy(false);
		sp.setPlaying(true);
		
		
		songPlayer.destroy();
		songPlayer = sp;
	}
}
