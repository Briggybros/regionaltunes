package com.toomuchminecraft.regionaltunes;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.toomuchminecraft.generalregionapi.api.Regions;
import com.xxmicloxx.NoteBlockAPI.Song;

public class RTCommandExecutor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (args.length != 0) {
			String subCommand = args[0];
			if (subCommand.equalsIgnoreCase("help") && args.length == 1) {
				if (sender.hasPermission("regionaltunes.help")) {
					sender.sendMessage(getHelpText());
					return true;
				} else {
					sender.sendMessage(ChatColor.RED
							+ "You do not have permission to do that!");
					return true;
				}
			} else if (subCommand.equalsIgnoreCase("reload")
					&& args.length == 1) {
				if (sender.hasPermission("regionaltunes.reload")) {
					RegionalTunes.getInstance().loadSongs();
					sender.sendMessage(ChatColor.GREEN + "Songs reloaded");
					return true;
				} else {
					sender.sendMessage(ChatColor.RED
							+ "You do not have permission to do that!");
					return true;
				}
			} else if (subCommand.equalsIgnoreCase("add") && (args.length == 3)) {
				if (sender.hasPermission("regionaltunes.add")) {
					Song song = RegionalTunes.getInstance().getSong(args[2]);

					if (Regions.doesRegionExist(args[1])) {
						if (song != null) {
							RegionalTunes.getInstance().addSong(args[1], song);
							sender.sendMessage(ChatColor.GREEN + "Song added!");
							return true;
						} else {
							sender.sendMessage(ChatColor.RED
									+ "That song does not exist!");
							return true;
						}
					} else {
						sender.sendMessage(ChatColor.RED
								+ "That region does not exist!");
						return true;
					}
				} else {
					sender.sendMessage(ChatColor.RED
							+ "You do not have permission to do that!");
					return true;
				}
			} else if (subCommand.equalsIgnoreCase("remove")
					&& (args.length == 3)) {
				if (sender.hasPermission("regionaltunes.remove")) {
					Song song = RegionalTunes.getInstance().getSong(args[2]);

					if (Regions.doesRegionExist(args[1])) {
						if (song != null) {
							RegionalTunes.getInstance().removeSong(args[1],
									song);
							sender.sendMessage(ChatColor.GREEN
									+ "Song removed!");
							return true;
						} else {
							sender.sendMessage(ChatColor.RED
									+ "That song does not exist!");
							return true;
						}
					} else {
						sender.sendMessage(ChatColor.RED
								+ "That region does not exist!");
						return true;
					}
				} else {
					sender.sendMessage(ChatColor.RED
							+ "You do not have permission to do that!");
					return true;
				}
			} else if (subCommand.equalsIgnoreCase("shuffle")
					&& (args.length == 2)) {
				if (sender.hasPermission("regionaltunes.shuffle")) {

					if (Regions.doesRegionExist(args[1])) {
						RegionalTunes.getInstance().toggleShuffle(args[1]);
						sender.sendMessage(ChatColor.GREEN + "Shuffle toggled!");
						return true;
					} else {
						sender.sendMessage(ChatColor.RED
								+ "That region does not exist!");
						return true;
					}
				} else {
					sender.sendMessage(ChatColor.RED
							+ "You do not have permission to do that!");
					return true;
				}
			} else if (subCommand.equalsIgnoreCase("list")
					&& (args.length == 2)) {
				if (sender.hasPermission("regionaltunes.list")) {

					if (Regions.doesRegionExist(args[1])) {
						RegionPlayer rp = RegionalTunes.getInstance()
								.getRegionPlayer(args[1]);

						sender.sendMessage(ChatColor.GRAY + "Songs: ");

						StringBuilder sb = new StringBuilder(ChatColor.GREEN
								+ "");

						for (Song s : rp.getSongList()) {
							sb.append(RegionalTunes.getNameFromSong(s) + ", ");
						}

						sb.setLength(sb.length() - 1);
						sender.sendMessage(sb.toString());

						return true;
					} else {
						sender.sendMessage(ChatColor.RED
								+ "That region does not exist!");
						return true;
					}
				} else {
					sender.sendMessage(ChatColor.RED
							+ "You do not have permission to do that!");
					return true;
				}
			} else if (subCommand.equalsIgnoreCase("library")
					&& (args.length == 1)) {
				if (sender.hasPermission("regionaltunes.library")) {
					sender.sendMessage(ChatColor.GRAY + "Library: ");

					StringBuilder sb = new StringBuilder(ChatColor.GREEN + "");

					for (Song s : RegionalTunes.songs.values()) {
						sb.append(RegionalTunes.getNameFromSong(s) + ", ");
					}

					sb.setLength(sb.length() - 1);
					sender.sendMessage(sb.toString());

					return true;
				} else {
					sender.sendMessage(ChatColor.RED
							+ "You do not have permission to do that!");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "Command not recognised.");
				return true;
			}
		} else if (sender.hasPermission("regionaltunes.help")) {
			sender.sendMessage(getHelpText());
			return true;
		} else {
			sender.sendMessage(ChatColor.RED
					+ "You do not have permission to do that!");
			return true;
		}
	}

	private String[] getHelpText() {
		return new String[] {
				new String(ChatColor.RED + "/rt help " + ChatColor.AQUA
						+ "- Bring up this menu"),
				new String(ChatColor.RED + "/rt reload " + ChatColor.AQUA
						+ "- Reload the songs"),
				new String(
						ChatColor.RED
								+ "/rt add "
								+ ChatColor.GOLD
								+ "[region] [song] "
								+ ChatColor.AQUA
								+ "- add 'song' to the playlist of 'region'"),
				new String(ChatColor.RED + "/rt remove " + ChatColor.GOLD
						+ "[region] [song] " + ChatColor.AQUA
						+ "- remove the song set for 'region'"),
				new String(
						ChatColor.RED
								+ "/rt shuffle "
								+ ChatColor.GOLD
								+ "[region] "
								+ ChatColor.AQUA
								+ "- Whether the songs for the region are played sequentially or randomly"),
				new String(ChatColor.RED + "/rt list " + ChatColor.GOLD
						+ "[region] " + ChatColor.AQUA
						+ "- Lists all of the songs added to the region"),
				new String(ChatColor.RED + "/rt library " + ChatColor.AQUA
						+ "- List all available songs") };
	}

}
