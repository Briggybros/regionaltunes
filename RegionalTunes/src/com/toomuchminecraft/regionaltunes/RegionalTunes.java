package com.toomuchminecraft.regionaltunes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.toomuchminecraft.generalregionapi.api.Regions;
import com.xxmicloxx.NoteBlockAPI.NBSDecoder;
import com.xxmicloxx.NoteBlockAPI.Song;
import com.xxmicloxx.NoteBlockAPI.SongPlayer;

public class RegionalTunes extends JavaPlugin {

	private static RegionalTunes instance;

	public static RegionalTunes getInstance() {
		return instance;
	}

	ArrayList<RegionPlayer> players = new ArrayList<RegionPlayer>();

	static HashMap<String, Song> songs = new HashMap<String, Song> ();

	public void onEnable() {
		loadFiles();
		loadSongs();
		loadSongList();

		this.getCommand("regionaltune").setExecutor(new RTCommandExecutor());

		this.getServer().getPluginManager()
				.registerEvents(new RTEventListener(), this);

		instance = this;
	}

	protected boolean isPlayerOurs(SongPlayer sp) {
		return getRegionPlayer(sp) != null;
	}

	protected RegionPlayer getRegionPlayer(String region) {
		for (RegionPlayer player : players)
			if (player.getRegion().equalsIgnoreCase(region))
				return player;
		return null;
	}

	protected RegionPlayer getRegionPlayer(SongPlayer songPlayer) {
		for (RegionPlayer player : players)
			if (player.getSongPlayer() == songPlayer)
				return player;
		return null;
	}

	public void loadFiles() {
		if (!this.getDataFolder().exists())
			this.getDataFolder().mkdir();

		File songFolder = new File(this.getDataFolder(), "tunes");
		if (!songFolder.exists())
			songFolder.mkdir();

	}

	public void loadSongs() {
		File songFolder = new File(this.getDataFolder(), "tunes");
		HashMap<String, Song> songList = new HashMap<String, Song>();

		for (File f : songFolder.listFiles()) {
			if (!f.isDirectory()) {
				Song s = NBSDecoder.parse(f);
				if (s != null)
					songList.put(f.getName().replaceAll(".nbs", ""), s);
			}
		}

		songs = songList;
	}

	public Song getSong(String string) {
		return songs.get(string);
	}

	public static String getNameFromSong(Song song) {
		for(Entry<String, Song> entry : songs.entrySet()) {
			if (entry.getValue() == song)
				return entry.getKey();
		}
		return null;
	}

	public void addSong(String region, Song song) {
		RegionPlayer rp = getRegionPlayer(region);
		if (rp != null) {
			if (!rp.contains(song)) {
				rp.addSong(song);
			}
		} else {
			rp = new RegionPlayer(region);
			rp.addSong(song);
			players.add(rp);
			
			for(Player p : Bukkit.getOnlinePlayers()) {
				if (Regions.isLocationInThisRegion(p.getLocation(), region))
					rp.addPlayer(p);
			}
		}
		saveSongList();
	}

	public void removeSong(String region, Song song) {
		RegionPlayer rp = getRegionPlayer(region);
		if (rp != null) {
			if (rp.contains(song))
				rp.removeSong(song);
		}
		saveSongList();
	}

	protected void onPlayerEnterRegion(String region, Player player) {
		RegionPlayer rp = getRegionPlayer(region);
		if (rp != null) {
			rp.addPlayer(player);
		}
	}

	protected void onPlayerLeaveRegion(String region, Player player) {
		RegionPlayer rp = getRegionPlayer(region);
		if (rp != null) {
			rp.removePlayer(player);
		}
	}

	protected void loadSongList() {
		try {
			players = new ArrayList<RegionPlayer>();
			File list = new File(this.getDataFolder(), "songset.txt");
			if (list.exists()) {
				BufferedReader br = new BufferedReader(new FileReader(list));
				String line;

				while ((line = br.readLine()) != null) {
					String region = line.split(":")[0];

					String songData = line.split(":")[1];
					songData = songData.substring(1, songData.length() - 1);

					String[] songs = songData.split(",");

					for (String s : songs)
						if (s != null && !s.equalsIgnoreCase(""))
							addSong(region, getSong(s));
				}

				br.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void saveSongList() {
		try {
			File list = new File(this.getDataFolder(), "songset.txt");
			list.delete();
			list.createNewFile();

			BufferedWriter bw = new BufferedWriter(new FileWriter(list));

			for (RegionPlayer rp : players) {
				bw.write(rp.getRegion() + ":[");

				for (Song song : rp.getSongList())
					bw.write(getNameFromSong(song) + ",");

				bw.write("]");
				bw.newLine();
			}

			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void toggleShuffle(String string) {
		getRegionPlayer(string).toggleShuffle();
	}
}
