package com.toomuchminecraft.regionaltunes;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.toomuchminecraft.generalregionapi.api.Regions;
import com.toomuchminecraft.generalregionapi.api.events.RegionEnteredEvent;
import com.toomuchminecraft.generalregionapi.api.events.RegionLeftEvent;
import com.xxmicloxx.NoteBlockAPI.SongEndEvent;

public class RTEventListener implements Listener {

	@EventHandler
	public void onRegionEntered(RegionEnteredEvent event) {
		RegionalTunes.getInstance().onPlayerEnterRegion(event.getRegionID(),
				event.getPlayer());
	}

	@EventHandler
	public void onRegionLeft(RegionLeftEvent event) {
		RegionalTunes.getInstance().onPlayerLeaveRegion(event.getRegionID(),
				event.getPlayer());
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		String[] regions = Regions.getRegionsFromLocation(event.getPlayer()
				.getLocation());

		for (String s : regions) {
			RegionalTunes.getInstance().onPlayerEnterRegion(s,
					event.getPlayer());
		}
	}

	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event) {
		String[] regions = Regions.getRegionsFromLocation(event.getPlayer()
				.getLocation());

		for (String s : regions) {
			RegionalTunes.getInstance().onPlayerLeaveRegion(s,
					event.getPlayer());
		}
	}

	@EventHandler
	public void onSongFinish(SongEndEvent event) {
		if (RegionalTunes.getInstance().isPlayerOurs(event.getSongPlayer()))
			RegionalTunes.getInstance().getRegionPlayer(event.getSongPlayer())
					.onSongFinished();
	}
}
